export interface User {
    
    uid?: string;
    name?: string;
    face_or_google?: boolean;
    email?: string;
    phone?: string;
    cpf?: string;
    photo?: string;
    password?: string;
    c_password?: string;
}