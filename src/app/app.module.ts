import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthProvider } from '../providers/auth';
import { PreferencesProvider } from '../providers/preferences';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { HttpClientModule } from '@angular/common/http';

//Imports para utilizar firebase
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { ValidateUserProvider } from '../providers/validate-user';
import { AngularFireAuth } from 'angularfire2/auth';
import { IonicStorageModule } from '@ionic/storage';
import { GoogleMaps } from '@ionic-native/google-maps';

import { Diagnostic } from '@ionic-native/diagnostic';
import { Geolocation } from '@ionic-native/geolocation';

//Initialize Firebase
export const firebase = {
    apiKey: "AIzaSyBzR1BxIJUcXmOlGIoh7PBkZNrN_-x8qi0",
    authDomain: "mapeando-b7e6a.firebaseapp.com",
    databaseURL: "https://mapeando-b7e6a.firebaseio.com",
    projectId: "mapeando-b7e6a",
    storageBucket: "mapeando-b7e6a.appspot.com",
    messagingSenderId: "785630430950",
    appId: "1:785630430950:web:bb0a0f83beeb7a31"
};


@NgModule({
  declarations: [
    MyApp,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AngularFireModule.initializeApp(firebase),
    AngularFireDatabaseModule,
    BrMaskerModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp

  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    PreferencesProvider,
    ValidateUserProvider,
    AngularFireAuth,
    GoogleMaps,
    Diagnostic,
    Geolocation
  ]
})
export class AppModule {}
