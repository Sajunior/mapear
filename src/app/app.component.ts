import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { PreferencesProvider } from '../providers/preferences';


export class MenuItem {
  title: string
  icon: string
  component: any
  hidden: boolean
  subs: MenuItem[]
}

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;

  pages: MenuItem[];

  constructor(
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public preferences: PreferencesProvider,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController
    
  ) {
    this.initializeApp();

    // used for an example of ngFor and navigation
    this.pages = [
      { title: 'Home', icon: 'home', component: 'HomePage', hidden:true,  subs:[]},
      { title: 'Filtro', icon: 'ios-navigate', component: null, hidden:true,  subs: [
        { title: 'Economico', icon: 'logo-usd', component: 'EconomicPage', hidden:true, subs: [] },
        //{ title: 'Comercio', icon: 'ios-cash', component: 'ComercioPage', hidden:true, subs: [] },
        { title: 'Imoveis', icon: 'custom-lotes', component: 'LotesCentroidesPage', hidden:true, subs: [] },
        //{ title: 'Imoveis', icon: 'ios-home', component: 'PropertiesPage', hidden:true, subs: [] }, 
        //{ title: 'Bairros', icon: 'custom-bairros', component: 'BairrosPage', hidden:true, subs: [] }
      ]},
      ]
    }

  initializeApp() {

    this.platform.ready().then(() => {
      //stuff if connected
      this.preferences.get().then((user) => {
        if (user) {
          //existe usúario gravado do preference então redireciona para ListPage
          this.rootPage = 'HomePage';
          //toda vez que o app for aberto atualiza o devicepreference e o array de mobile
          this.hideSplashscreen();
        } else {
          this.rootPage = 'SigninPage';
          this.hideSplashscreen();
        }
      })
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.backgroundColorByHexString("#000")
      this.splashScreen.hide();
    });
  }

  private hideSplashscreen(): void {
    if (this.splashScreen) {
      setTimeout(() => {
        this.splashScreen.hide();
      }, 500);
    }
  }

  hideMenus(){
    for (let key in this.pages) { this.pages[key].hidden = true }
  }



  openPage(page:MenuItem) {
    if(page.subs.length > 0){
      page.hidden = !page.hidden
    } else {
      this.nav.setRoot(page.component).then(
        res => {
          this.menuCtrl.close()
          this.hideMenus()
        }
      )
    }
  }

  logout() {
    this.preferences.remove();
    this.nav.setRoot('SigninPage');
    this.menuCtrl.close()
  }

  presentConfirm() {
    let alert = this.alertCtrl.create({
      title: 'Alerta',
      message: 'Deseja mesmo deslogar?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Sim',
          handler: () => {
            this.logout()
          }
        }
      ]
    });
    alert.present();
  }
  
}
