import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { User } from '../models/user'
import { PreferencesProvider } from './preferences';


@Injectable()
export class AuthProvider {

  constructor(
    public http: HttpClient,
    public db: AngularFireDatabase,
    public af: AngularFireAuth,
    public preferences: PreferencesProvider
  ) {
    //code here..
  }

  signInWithEmail(credentials):Promise<any>{
    return new Promise((resolve, reject, ) => {
      this.getEmailByCPF(credentials.cpf).then((email) => {
        this.af.auth.signInWithEmailAndPassword(email, credentials.password).then((data) => {
          data.user.uid

          this.getUserInDatabase(data.user.uid).then((user)=> {
              this.preferences.create(user).then(() => {
                resolve('success')
              })
          }).catch((err) => {
            console.log(err)
            reject('Falha ao logar')
          })
        }).catch((err) => {
          reject('Falha ao logar')
        })
      }).catch((err) => {
        console.log('err', err)
        reject('Erro, não existe conta com esse cpf')
      })
    });
    
  }

  getEmailByCPF(cpf): Promise<any> {
    return new Promise((resolve, reject) => {
      return this.db.database.ref('cpfs').orderByChild('cpf').equalTo(cpf).on("value", function(snapshot){
        if(snapshot.exists()){
          snapshot.forEach(function(data) {
            let email = data.val().email;
            resolve(email)
          })
        } else {
          reject('err')
        }
      })
    })
  }


  signUpWithEmail(user) {
    console.log(user)
    console.log(user.value)
    return this.af.auth.createUserWithEmailAndPassword(user.email, user.password)
      .then(() => {
        let uid = this.af.auth.currentUser.uid;
        return this.db.database.ref('users').child(uid).set({
          name: user.name,
          email: user.email,
          photo: user.photo ? user.photo : '',
          phone: user.phone ? user.phone : '',
          cpf: user.cpf,
          face_or_google: false,
        }).then(() => {
          //Gravando em um nó de cpfs para fazer login com cpf, e poder resetar senha no forgetPassword
          return this.db.database.ref('cpfs').child(uid).set({
            email: user.email,
            cpf: user.cpf
          }).then(() => {

            let userstorage = { 
              nome: user.name,
              email: user.email,
              cpf: user.cpf,
              uid: uid
            }

            this.preferences.create(userstorage)

          }
        )
        })
      })
  }


  getUserInDatabase(uid): Promise<any> {

    return new Promise((resolve, reject) => {
      this.db.database.ref('users').child(uid).on('value', (snapshot) => {
        let result = snapshot.val()
        let user: User = {
          uid: uid,
          name: result.name ? result.name : '',
          email: result.email ? result.email : '',
          cpf: result.cpf ? result.cpf : '',
          phone: result.phone ? result.phone : '',
          face_or_google: result.face_or_google ? result.face_or_google : false,
          photo: result.photo? result.photo : '',
        }
        resolve(user)
        console.log('criado o preference', JSON.stringify(user))
      },err => {
        reject(err)
      });
    })
  }




}
