import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, AlertController, Loading, LoadingController } from 'ionic-angular';
import { GoogleMapOptions, GoogleMaps, GoogleMapsEvent, GoogleMap, ILatLng, Circle } from '@ionic-native/google-maps';
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-economic',
  templateUrl: 'economic.html',
})

export class EconomicPage {
  //variaveis globais

  map: GoogleMap;

  uid: string;
  task;
  list_task_new = []
  list_task_finish = []
  searchTerm: string;
  alterViewNotNetWork = false

  task_seccion = "task_new"

  items = [
    'Amsterdam',
    'Bogota',
  ];

  request = {
    word: ''
  }

  /*urls = [
    'https://jsonplaceholder.typicode.com/todos/1',
    'https://jsonplaceholder.typicode.com/todos/2',
    'https://jsonplaceholder.typicode.com/todos/3'
  ];*/

  urls = [
    { item: 'name' },
    { item: 'name' }
  ]




  economico = [];
  economico_filter = [];
  economico_filter_next_page: string;
  filter: boolean = false;
  load: Loading;
  nextpage: any;
  lastpage: any;

  url: string = 'http://34.68.95.171/mapeamento_api/public/api/';
  url2: string = 'http://localhost:8000/api/';

  lat: any;
  long: any;
  gotPosition: boolean = false;

  position = {
    lat: 0,
    lng: 0
  }

  gaveLocationPermission: boolean = false;
  hasEnabledLocation: boolean = false;
  doneChecking: boolean = false;
  hasRequestedLocation: boolean = false;

  //fim -variaveis globais

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public http: HttpClient,
    public dgt: Diagnostic,
    public g: Geolocation,
    public plt: Platform,
  ) {
    this.locationIos()
  }

  getPosition() {
    this.plt.ready().then(() => {
      this.g.getCurrentPosition().then((resp) => {
        this.lat = resp.coords.latitude;
        this.long = resp.coords.longitude;
        this.gotPosition = true;

        this.position = {
          lat: resp.coords.latitude,
          lng: resp.coords.longitude
        }

        this.loadGoogleMap()

      }).catch((error) => {
        console.log('Error getting location', error);
      });
    });
  }

  loadGoogleMap() {

    this.load = this.loadingCtrl.create({
      content: "Atualizando dados"
    });
    this.load.present();

    /*let body = {
      lat_: this.position.lat,
      lng: this.position.lng
    }*/

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.position.lat,
          lng: this.position.lng
        },
        zoom: 17,
        tilt: 30
      }
    };


    this.http.post<any[]>(this.url + 'lotes/raio', { lat_: this.position.lat, lng: this.position.lng }).subscribe((res) => {
      let GOOGLE: ILatLng = { "lat": this.position.lat, "lng": this.position.lng };
      this.map = GoogleMaps.create('map_canvas', mapOptions);

      this.map.one(GoogleMapsEvent.MAP_READY)
        .then(() => {
          this.map.addCircle({
            'center': GOOGLE,
            'radius': 100,
            'strokeColor': 'rgba(0, 0, 255, 0.75)',
            'strokeWidth': 1,
            'fillColor': 'rgba(0, 0, 255, 0.1)',
            'visible': true
          }).then((circle: Circle) => {
            console.log('chamando add MarkerArray');
            this.addMarkerArray(res);
          }).then(() => {

          }).catch((err) => {

            console.log(err)
          })
        });
    }, err => {
      this.load.dismiss();
    })
  }



  locationIos() {

    let authorizationLoader = this.loadingCtrl.create({
      content: "Checando permissões..."
    });
    authorizationLoader.present();

    this.plt.ready().then(() => {
      this.dgt.isLocationAuthorized().then((resp) => {
        authorizationLoader.dismiss().then(() => {
          this.gaveLocationPermission = resp;
          if (this.gaveLocationPermission) {
            let enablingLoader = this.loadingCtrl.create({
              content: "Checando status do GPS..."
            });
            enablingLoader.present();
            console.log('Gave location permission')
            this.dgt.isLocationEnabled().then((resp) => {
              console.log('Location is enabled : ' + resp)
              enablingLoader.dismiss()
              console.log('Enabling loader dismissed')
              this.doneChecking = true;
              this.hasEnabledLocation = resp;
              if (this.hasEnabledLocation) {
                this.getPosition();
              }
            });
          } else {
            this.gotPosition = false
            //this.resetUBSDistance()
            this.lat = null
            this.long = null
            if (!this.hasRequestedLocation) {
              this.hasRequestedLocation = true
              this.dgt.requestLocationAuthorization().then((res) => {
                if (res != 'denied') {
                  console.log('Permission : ' + res)
                  this.dgt.isLocationAuthorized().then((res) => {
                    this.gaveLocationPermission = res;
                    if (this.gaveLocationPermission) {
                      let enablingLoader = this.loadingCtrl.create({
                        content: "Checando status do GPS..."
                      });
                      //  enablingLoader.present();
                      this.dgt.isLocationEnabled().then((resp) => {
                        enablingLoader.dismiss()
                        this.doneChecking = true;
                        this.hasEnabledLocation = resp;
                        if (this.hasEnabledLocation) {
                          this.getPosition();
                        }
                      });
                    }
                  })
                } else { // denied location

                }
              })
            }
            this.doneChecking = true;
          }
        });
      }, err => {
        this.gotPosition = false;
        //this.resetUBSDistance()
        this.lat = null;
        this.long = null;
      });
    })


  }



  async addMarkerArray(array_lotes_coords) {

    console.log('addMarkerArray', array_lotes_coords)
    for (const [idx, element] of array_lotes_coords.entries()) {
      let marker = await this.map.addMarker({
        //title: 'Localização atual',
        icon: 'red',
        animation: 'DROP',
        position: {
          lat: element.lat,
          lng: element.long
        }
      });

      marker.addEventListener(GoogleMapsEvent.MARKER_CLICK)

      .subscribe(res => this.navCtrl.push('EconomicDetalhePage', {item: element }), err => console.log(err))

      console.log(`Received Todo ${idx + 1}:`);
    }

    this.load.dismiss();
    console.log('Finished!');
  }

}



