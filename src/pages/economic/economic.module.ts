import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EconomicPage } from './economic';

@NgModule({
  declarations: [
    EconomicPage,
  ],
  imports: [
    IonicPageModule.forChild(EconomicPage),
  ],
})
export class EconomicPageModule {}
