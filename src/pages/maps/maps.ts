import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Loading, LoadingController, AlertController } from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  ILatLng,
  Circle,
} from '@ionic-native/google-maps';


@IonicPage()
@Component({
  selector: 'page-maps',
  templateUrl: 'maps.html',
})
export class MapsPage {
  map: GoogleMap;
  position = {
    lat: 0,
    lng: 0
  }
  load: Loading;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController) {


    this.position.lat = this.navParams.get('lat')
    this.position.lng = this.navParams.get('long')

    console.log('this.position.lat', this.position.lat, 'this.position.lng ', this.position.lng)

    this.loadingPresent()

   // this.loadGoogleMap()

    this.loadMap()


  }


  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.position.lat,
          lng: this.position.lng
        },
        zoom: 18,
        tilt: 30
      }
    };
    console.log(this.position.lat)
    let GOOGLE: ILatLng = { "lat": this.position.lat, "lng": this.position.lng };
    this.map = GoogleMaps.create('map_canvas', mapOptions);

    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        // Add circle
        this.map.addCircle({
          'center': GOOGLE,
          'radius': 100,
          'strokeColor' : 'rgba(0, 0, 255, 0.75)',
          'strokeWidth': 1,
          'fillColor' : 'rgba(0, 0, 255, 0.1)',
          'visible': true
        }).then((circle: Circle) => {
          this.map.addMarker({
            title: 'Localização atual',
            icon: 'red',
            animation: 'DROP',
            position: {
              lat: this.position.lat,
              lng: this.position.lng
            }
          })
          .then(() => {
            this.load.dismiss()
          }).catch((err) => {
            this.load.dismiss()
            console.log(err)
          })
        });


      }).catch(err => { console.log('err 1', err), this.load.dismiss() })


  }



  loadGoogleMap() {
    console.log('Tentando criar mapa')
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.position.lat,
          lng: this.position.lng
        },
        zoom: 18,
        tilt: 30
      }
    };

    console.log('tudo ok 1')
    this.map = GoogleMaps.create('map_canvas', mapOptions);

    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('tudo ok 2')

        this.map.addMarker({
          title: 'Localização atual',
          icon: 'red',
          animation: 'DROP',
          position: {
            lat: this.position.lat,
            lng: this.position.lng
          }
        })
          .then(() => {
            this.load.dismiss()
          }).catch((err) => {
            this.load.dismiss()
            console.log(err)
          })

      }).catch(err => { console.log('err 1', err), this.load.dismiss() })
  }



  loadingPresent() {
    this.load = this.loadingCtrl.create({
      content: "Carregando mapa.."
    })
    this.load.present()
  }

  alertPresent(title, message) {
    let alert = this.alertCtrl.create({
      title: title,
      message: message
    })

    return alert.present();
  }

  sugestao() {
    this.navCtrl.push('SugestoesPage')
  }



}