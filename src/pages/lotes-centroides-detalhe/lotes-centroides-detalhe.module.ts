import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LotesCentroidesDetalhePage } from './lotes-centroides-detalhe';

@NgModule({
  declarations: [
    LotesCentroidesDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(LotesCentroidesDetalhePage),
  ],
})
export class LotesCentroidesDetalhePageModule {}
