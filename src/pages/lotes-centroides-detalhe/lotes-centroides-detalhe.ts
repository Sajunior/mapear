import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, AlertController } from 'ionic-angular';

/**
 * Generated class for the LotesCentroidesDetalhePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lotes-centroides-detalhe',
  templateUrl: 'lotes-centroides-detalhe.html',
})
export class LotesCentroidesDetalhePage {

  item: any

  constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public alertCtrl: AlertController,
     public modalCtrl: ModalController
     ) {
    this.item = this.navParams.get('item')
    console.log(this.item)
  }


  
  showAlert() {
    const alert = this.alertCtrl.create({
      
      title: 'Lat,Lon',
      message: this.item['lat'] + `<br>` + this.item['long'],
      buttons: [{text: 'OK', role: 'cancel'}]
    });
    alert.present();
  }

  openMap(){
    this.navCtrl.push('MapsPage', {lat:this.item['lat'], long:this.item['long']});


  }

}