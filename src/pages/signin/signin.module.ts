import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SigninPage } from './signin';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    SigninPage,
  ],
  imports: [
    IonicPageModule.forChild(SigninPage),
    BrMaskerModule
  ],
  exports: [
    BrMaskerModule
  ]
})
export class SigninPageModule {}
