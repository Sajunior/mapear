import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, Loading } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth';
import { PreferencesProvider } from '../../providers/preferences';
import { FormGroup, FormBuilder, Validators  } from '@angular/forms';


@IonicPage()
@Component({
  selector: 'page-signin',
  templateUrl: 'signin.html',
})
export class SigninPage {

  signinForm: FormGroup;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    private loadingCtrl: LoadingController,
    private auth: AuthProvider,
    private preferences: PreferencesProvider,
    private formBuilder: FormBuilder
  )
  {          
    this.signinForm = this.formBuilder.group({
      cpf: ['', [ Validators.required, Validators.minLength(14) ]],
      password: ['', [ Validators.required, Validators.minLength(6) ]]
    });
  }

  onSubmit(): void {
    let loading: Loading = this.showLoading();

    this.auth.signInWithEmail(this.signinForm.value)
      .then(() => {
        loading.dismiss();
        this.navCtrl.setRoot('HomePage');
      }).catch(err => {
        loading.dismiss();
        this.presentAlert(err)
      })
  }


  gotoRegisterPage() {
    this.navCtrl.push('SignupPage')
  }

  teste(){
    this.auth.getUserInDatabase('hsapqowoqẃqṕ').then((bunda)=>{
      console.log(bunda)
    }).catch((peito)=> {
      console.log(peito)
    })
  }

  /*
  facebookLogin() {
    this.facebook.login(['email'])
      .then(response => {
        const facebookCredentials = auth.FacebookAuthProvider
          .credential(response.authResponse.accessToken);
          let loading: Loading = this.showLoading();
          this.auth.signInWithFacebook(facebookCredentials)
            .then(() => {
              loading.dismiss();
              this.navCtrl.setRoot('HomePage')
            }).catch(err => {
              this.presentAlert('Ocorreu um erro!')
              loading.dismiss()
            })
      }).catch(err => {
        console.log(err)
      })
  }
  */
  
  /*
  googlePlusLogin(){
    this.googlePlus.login({
      'webClientId': '561814281640-itercr5gcsg58oi1gqpkfe08fnlugvo8.apps.googleusercontent.com',
      'offline': true
    }).then(response => {
      let loading: Loading = this.showLoading();
      this.auth.signInWithGooglePlus(response)
        .then(res => {
          loading.dismiss()
          this.navCtrl.setRoot('HomePage');
        }).catch(err => {
          loading.dismiss()
          this.presentAlert('Ocorreu um erro!')
        })
    }).catch(err => console.log(err + 'erro'))
  }
  */

  private showLoading(): Loading{
    let loading: Loading = this.loadingCtrl.create({
      content: 'Aguarde um momento...'
    });

    loading.present();

    return loading;
  }

  private presentAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Ocorreu um erro',
      subTitle: message,
      buttons: ['Ok']
    });
    alert.present();
  }
}


