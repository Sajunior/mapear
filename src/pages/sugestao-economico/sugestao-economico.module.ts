import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SugestaoEconomicoPage } from './sugestao-economico';

@NgModule({
  declarations: [
    SugestaoEconomicoPage,
  ],
  imports: [
    IonicPageModule.forChild(SugestaoEconomicoPage),
  ],
})
export class SugestaoEconomicoPageModule {}
