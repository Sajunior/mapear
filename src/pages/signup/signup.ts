import { Component } from '@angular/core';
import { NavController, NavParams, Platform, LoadingController, Loading, AlertController, IonicPage } from 'ionic-angular';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { AuthProvider } from '../../providers/auth';
import { PreferencesProvider } from '../../providers/preferences';
import { ValidateUserProvider } from '../../providers/validate-user';

import { AngularFireDatabase } from 'angularfire2/database';


@IonicPage()
@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {

  result: any;

  load: Loading

  registerForm: FormGroup;

  public tipoSenha: string = 'password'
  public visaoSenha: string = 'ios-eye-outline'

  public tipoConfirmaSenha: string = 'password'
  public visaoConfirmaSenha: string = 'ios-eye-outline'

  messageEmail = ""
  messagePassword = "";
  messageCpf = "";
  messageName = "";
  errorName = false;
  errorEmail = false;
  errorPassword = false;
  errorCpf = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public formBuilder: FormBuilder,
    public platform: Platform,
    public alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public auth: AuthProvider,
    public preferences: PreferencesProvider,
    public validateUser: ValidateUserProvider,
    public db: AngularFireDatabase,

  ) {
    let cpfIsValid = (control: FormControl) => {
      //vai verificar se o cpf é valido, se não for retorna um {"cpf_invalid": true} e o campo fica invalido
      if(validateUser.validatorCpf(control.value)){
        return null
      } else {
        return {"cpf_invalid": true}
      }
    }

    this.registerForm = this.formBuilder.group({
      name: ['', [Validators.required, Validators.minLength(3), Validators.pattern('[a-zA-Z ]*')]],
      email: ['', Validators.compose([Validators.required, Validators.maxLength(70), Validators.pattern('^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'), Validators.required])],
      cpf: ['', Validators.compose([Validators.required, Validators.minLength(14), cpfIsValid])],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }


  onSubmit() {
    let { email, password, cpf, name } = this.registerForm.controls;

    if (!this.registerForm.valid) {

      if (!email.valid) {
        this.errorEmail = true;
        this.messageEmail = "Ops! Email inválido";
      } else {
        this.messageEmail = "";
      }

      if (!password.valid) {
        this.errorPassword = true;
        this.messagePassword = "A senha precisa ter de 6 a 20 caracteres"
      } else {
        this.messagePassword = "";
      }
      if (!cpf.valid) {
        this.errorCpf = true;
        this.messageCpf = "Digite um cpf válido";
      } else {
        this.errorCpf = false;
        this.messageCpf = " ";
      }

      if (!name.valid) {
        this.errorName = true;
        this.messageName = "O nome deve ter no minimo 3 caracteres"
      } else {
        this.errorName = false;
        this.messageName = " "
      }
    } else {
      this.errorName = false;
      this.errorEmail = false;
      this.errorPassword = false;
      this.errorCpf = false;
      console.log('tudo ok!')
      this.load = this.showLoading()
      this.load.present()
      //fazer cadastro
      //verificar se já possui cpf cadastrado
      this.validateUser.verifyCPF(cpf.value).then(() => {
        this.auth.signUpWithEmail(this.registerForm.value).then(() => {
          this.load.dismiss()
          this.navCtrl.setRoot('SigninPage')
        }).catch((erro) => {
          this.load.dismiss();
          if (erro.code == 'auth/weak-password') {
            this.alert('A sua senha deve ter no minimo 6 caracteres.')
          } else if (erro.code == 'auth/email-already-in-use') {
            this.alert('O e-mail digitado já esta em uso.')
          } else if (erro.code == 'auth/invalid-email') {
            this.alert('O e-mail digitado não é valido.')
          } else if (erro.code == 'auth/operation-not-allowed') {
            this.alert('Não esta habilitado criar usúarios.')
          } else if (erro.code == 'auth/network-request-failed') {
            this.alert('Verifique sua conexão')
          } else {
            this.alert(erro.messsage)
          }
        })
      }).catch(() => {
        this.load.dismiss()
        console.log('catch cpf existi')
        this.alert('Já existi uma conta com esse cpf em nossos registros')
      })
    }
  }


  public toggleVisaoSenha() {
    if (this.tipoSenha == 'password') {
      this.visaoSenha = 'ios-eye-off-outline'
      this.tipoSenha = 'text'
    }
    else {
      this.visaoSenha = 'ios-eye-outline'
      this.tipoSenha = 'password'
    }
  }

  private showLoading(): Loading {
    let loading: Loading = this.loadingCtrl.create({
      content: 'Aguarde um momento...'
    });

    loading.present();

    return loading;
  }


  private alert(message) {
    let alert = this.alertCtrl.create({
      title: 'Ocorreu um erro',
      subTitle: message,
      buttons: ['Ok']
    });
    alert.present();
  }


  validateField() {

    let { email, password, cpf, name } = this.registerForm.controls;

    if (!email.valid) {
      this.errorEmail = true;
      this.messageEmail = "Ops! Email inválido";
    } else {
      this.messageEmail = "";
    }

    if (!password.valid) {
      this.errorPassword = true;
      this.messagePassword = "A senha precisa ter de 6 a 20 caracteres"
    } else {
      this.messagePassword = "";
    }
    if (!cpf.valid) {
      this.errorCpf = true;
      this.messageCpf = "Digite um cpf válido";
    } else {
      this.errorCpf = false;
      this.messageCpf = " ";
    }

    if (!name.valid) {
      this.errorName = true;
      this.messageName = "O nome deve ter no mínimo 3 caracteres"
    } else {
      this.errorName = false;
      this.messageName = " "
    }
  }


}
