import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SugestoesPage } from './sugestoes';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    SugestoesPage,
  ],
  imports: [
    IonicPageModule.forChild(SugestoesPage),
    BrMaskerModule
  ],
  exports: [
    BrMaskerModule
  ]
})
export class SugestoesPageModule {}
