import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController, Loading } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';


@IonicPage()
@Component({
  selector: 'page-sugestoes',
  templateUrl: 'sugestoes.html',
})
export class SugestoesPage {

  item: any

  constructor(
    public http: HttpClient, 
    public navParams: NavParams,
  ) {
    this.item = navParams.get('item')
    console.log(this.item)
    
    http.get("https://viacep.com.br/ws/0001111/json").subscribe((res) => {
      console.log(res)
    }, error => {
      console.log(error)
     }
    )
  
  }
}

