import { Geolocation } from '@ionic-native/geolocation';
import { Diagnostic } from '@ionic-native/diagnostic';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, Alert, AlertController } from 'ionic-angular';
import { GoogleMapOptions, GoogleMaps, GoogleMapsEvent, GoogleMap, ILatLng, Circle} from '@ionic-native/google-maps';

/**
 * Generated class for the HomePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  map: GoogleMap;

  lat: any;
  long: any;
  gotPosition: boolean = false;

  position = {
    lat: 0,
    lng: 0
  }

  gaveLocationPermission: boolean = false;
  hasEnabledLocation: boolean = false;
  doneChecking: boolean = false;
  hasRequestedLocation: boolean = false;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public d: Diagnostic,
    public g: Geolocation,
    public plt: Platform,
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController

    ) {

      this.locationIos()
  }


  locationIos() {


    let authorizationLoader = this.loadingCtrl.create({
      content: "Checando permissões..."
    });
    authorizationLoader.present();

    this.plt.ready().then(() => {
      
      this.d.isLocationAuthorized().then((resp) => {
        authorizationLoader.dismiss().then(() => {
          this.gaveLocationPermission = resp;
          if (this.gaveLocationPermission) {
            let enablingLoader = this.loadingCtrl.create({
              content: "Checando status do GPS..."
            });
            enablingLoader.present();
            console.log('Gave location permission')
            this.d.isLocationEnabled().then((resp) => {
              console.log('Location is enabled : ' +resp)
              enablingLoader.dismiss()
              console.log('Enabling loader dismissed')
              this.doneChecking = true;
              this.hasEnabledLocation = resp;
              if (this.hasEnabledLocation) {
                this.getPosition();
              }
            });
          } else {
            this.gotPosition = false
            //this.resetUBSDistance()
            this.lat = null
            this.long = null
            if(!this.hasRequestedLocation) {
              this.hasRequestedLocation = true
              this.d.requestLocationAuthorization().then((res)=>{
                if(res!='denied'){
                  console.log('Permission : '+ res)
                  this.d.isLocationAuthorized().then((res)=>{
                    this.gaveLocationPermission = res;
                    if (this.gaveLocationPermission) {
                      let enablingLoader = this.loadingCtrl.create({
                        content: "Checando status do GPS..."
                      });
                    //  enablingLoader.present();
                      this.d.isLocationEnabled().then((resp) => {
                        enablingLoader.dismiss()
                        this.doneChecking = true;
                        this.hasEnabledLocation = resp;
                        if (this.hasEnabledLocation) {
                          this.getPosition();
                        }
                      });
                    }
                  })
                } else { // denied location

                }
              })
            }
            this.doneChecking = true;
          }
        });
      }, err =>{
        this.gotPosition = false
        //this.resetUBSDistance()
        this.lat = null
        this.long = null
      });
    })


  }

  getPosition() {
    this.plt.ready().then(() => {
      this.g.getCurrentPosition().then((resp) => {
        this.lat = resp.coords.latitude;
        this.long = resp.coords.longitude;
        this.gotPosition = true;

        this.position = {
          lat: resp.coords.latitude,
          lng: resp.coords.longitude
        }

        this.loadGoogleMap()

      }).catch((error) => {
        console.log('Error getting location', error);
      });
    });
  }
  
  loadGoogleMap() {
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.position.lat,
          lng: this.position.lng
        },
        zoom: 18,
        tilt: 30
      }
    };

    console.log('tudo ok 1')
    let GOOGLE: ILatLng = {"lat" :this.position.lat, "lng" : this.position.lng};
    this.map = GoogleMaps.create('map_canvas', mapOptions);
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('tudo ok 2')
        this.map.addCircle({
          'center': GOOGLE,
          'radius': 250,
          'strokeColor' : 'rgba(0, 0, 255, 0.75)',
          'strokeWidth': 1,
          'fillColor' : 'rgba(0, 0, 255, 0.1)',
          'visible': true
        }).then((circle: Circle) => {
          this.map.addMarker({
          title: 'Localização atual',
          icon: 'red',
          animation: 'DROP',
          position: {
            lat: this.position.lat,
            lng: this.position.lng
          }
        })
          .then(() => {
       
          }).catch((err) => {
              
            console.log(err)
          })
      });

      }).catch(err => {console.log('err 1', err)})


  }

  sugestao(){
    this.navCtrl.push('SugestoesPage')
  }


}
