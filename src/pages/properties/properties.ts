import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController, Loading } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the PropertiesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-properties',
  templateUrl: 'properties.html',
})
export class PropertiesPage {

  load1: Loading
uid: string;
task;
list_task_new = []
list_task_finish = []
searchTerm: string;
alterViewNotNetWork = false


task_seccion = "task_new"

items = [
  'Amsterdam',
  'Bogota',
];


propiedades = [];
load: Loading;
nextpage: any;
lastpage: any;

//fim -variaveis globais

constructor(
  public navCtrl: NavController,
  public navParams: NavParams,
  public loadingCtrl: LoadingController,
  public modalCtrl: ModalController,
  public alertCtrl: AlertController,
  public http: HttpClient,
) {

  this.loadingPresent()

  http.get("http://localhost:8000/api/economico").subscribe((data) => {
    this.load.dismiss()
    console.log(data)
    console.log(data[0]['data'][1]['Nome Fantasia'])
    this.nextpage = data[0]['next_page_url'];
    this.propiedades
   = data[0]['data']
  }, error => {
    this.load.dismiss() 
    console.log(error)
   }
  )
  
}

//métodos aqui
filterItems(data){
  console.log(this.searchTerm)
  let request = {
    word:this.searchTerm.toUpperCase()
  }
  this.http.post("http://localhost:8000/api/economicobyname",request).subscribe((data) => {
    console.log(data)
    this.propiedades
   = data[0]["data"];


    //arrayBairros.forEach(element => {
    //  this.propiedades
  }, error => {
    console.log(error)
   })
}

cancelSearch() {
  console.log('cancel search')
}

searchByCity() {
  console.log('search by city')
  console.log(this.searchTerm)

  this.items;

  this.items.forEach(element => {
    if (element.toLowerCase() == this.searchTerm.toLowerCase()) {
      console.log("item", element)
    } else {
      console.log("nenhum elemento encontrado com esse nome")
    }
  })
}


doInfinite(infinity) {
  if(this.nextpage != null){
    this.http.get(this.nextpage).subscribe((data) => {
      this.nextpage = data[0]['next_page_url'];
      console.log(data)
      infinity.complete()
      let arrayBairros = data[0]['data'];


      arrayBairros.forEach(element => {
        this.propiedades
      .push(element)
      });

    }, error => {
      infinity.complete()
      console.log(error)
     })
  }else {infinity.complete()}

}

loadingPresent(){
  this.load = this.loadingCtrl.create({
    content: "carregando dados.."
  })

  this.load.present()
}

getDetail(item){
  //console.log(item)
  this.navCtrl.push('EconomicDetalhePage', {item:item})
}
//ate aqui - metodos

}
