import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, ModalController, AlertController, Loading } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-comercio',
  templateUrl: 'comercio.html',
})
export class ComercioPage {

  //variaveis globais
load1: Loading
uid: string;
task;
list_task_new = []
list_task_finish = []
searchTerm: string;
alterViewNotNetWork = false

task_seccion = "task_new"

items = [
  'Amsterdam',
  'Bogota',
];

request = {
  word: ''
}


comercios = [];
comercios_filter = [];
comercios_filter_next_page: string;
filter: boolean = false;
load: Loading;
nextpage: any;
lastpage: any;

url: string = 'http://192.168.1.132:8000/api/';
url2: string = 'http://localhost:8000/api/';

//fim -variaveis globais

constructor(
  public navCtrl: NavController,
  public navParams: NavParams,
  public loadingCtrl: LoadingController,
  public modalCtrl: ModalController,
  public alertCtrl: AlertController,
  public http: HttpClient,
) {

  this.loadingPresent()

  http.get(this.url + "comercio").subscribe((data) => {
    this.load.dismiss()
    console.log(data)
    console.log(data[0]['data'][1]['bairro'])
    this.nextpage = data[0]['next_page_url'];
    this.comercios = data[0]['data']
  }, error => {
    this.load.dismiss() 
    console.log(error)
   }
  )

}

//métodos aqui
filterItems(data){
  console.log(this.searchTerm)

  if(this.searchTerm != ''){
    this.filter = true
  } else {
    this.filter = false
  }


  this.request.word = this.searchTerm.toUpperCase()
  this.http.post(this.url + "comerciobyname",this.request).subscribe((data) => {
    console.log(data)
    this.comercios_filter = data[0]["data"];
    this.comercios_filter_next_page = data[0]["next_page_url"];


    //arrayBairros.forEach(element => {
    //  this.comercios.push(element)
    //});
  }, error => {
    console.log(error)
   })
}

cancelSearch() {
  console.log('cancel search')
}




doInfinite(infinity) {
  if(this.nextpage != null){
    this.http.get(this.nextpage).subscribe((data) => {
      this.nextpage = data[0]['next_page_url'];
      console.log(data)
      infinity.complete()
      let arrayBairros = data[0]['data'];


      arrayBairros.forEach(element => {
        this.comercios.push(element)
      });

    }, error => {
      infinity.complete()
      console.log(error)
     })
  }else {infinity.complete()}

}

doInfiniteFilter(infinity) {
  if(this.nextpage != null){
    this.http.post(this.comercios_filter_next_page + '&word=' + this.request.word, this.request.word).subscribe((data) => {
      this.comercios_filter_next_page = data[0]['next_page_url'];
      console.log(data)
      infinity.complete()
      let arrayBairros = data[0]['data'];


      arrayBairros.forEach(element => {
        this.comercios_filter.push(element)
      });

    }, error => {
      infinity.complete()
      console.log(error)
     })
  }else {infinity.complete()}

}

loadingPresent(){
  this.load = this.loadingCtrl.create({
    content: "carregando dados.."
  })

  this.load.present()
}

openMap(item){
  //console.log(item)
  this.navCtrl.push('MapsPage', {lat:item["lat"], long: item["long"]})
}


searchByCity() {
  console.log('search by city')
  console.log(this.searchTerm)

  this.items;

  this.items.forEach(element => {
    if (element.toLowerCase() == this.searchTerm.toLowerCase()) {
      console.log("item", element)
    } else {
      console.log("nenhum elemento encontrado com esse nome")
    }
  })
}


//ate aqui - metodos
}

