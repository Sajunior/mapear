import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';

/**
 * Generated class for the BairrosDetalhePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-bairros-detalhe',
  templateUrl: 'bairros-detalhe.html',
})
export class BairrosDetalhePage {

  item: any

  constructor(
    public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
  ){
    this.item = this.navParams.get('item')
    console.log(this.item)
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BairrosDetalhePage');
  }

  showAlert() {
    const alert = this.alertCtrl.create({
      
      title: 'Geometria',
      message: this.item['the_geom'],
      buttons: [{text: 'OK', role: 'cancel'}]
    });
    alert.present();
  }

}
