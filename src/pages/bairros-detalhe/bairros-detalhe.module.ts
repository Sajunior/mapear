import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BairrosDetalhePage } from './bairros-detalhe';

@NgModule({
  declarations: [
    BairrosDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(BairrosDetalhePage),
  ],
})
export class BairrosDetalhePageModule {}
