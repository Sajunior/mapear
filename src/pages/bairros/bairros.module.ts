import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BairrosPage } from './bairros';

@NgModule({
  declarations: [
    BairrosPage,
  ],
  imports: [
    IonicPageModule.forChild(BairrosPage),
  ],
})
export class BairrosPageModule {}
