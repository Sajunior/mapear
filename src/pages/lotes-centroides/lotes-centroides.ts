import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, LoadingController, Loading } from 'ionic-angular';
import { HttpClient } from '@angular/common/http';

/**
 * Generated class for the LotesCentroidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lotes-centroides',
  templateUrl: 'lotes-centroides.html',
})
export class LotesCentroidesPage {

  //variaveis globais
load1: Loading
uid: string;
task;
list_task_new = []
list_task_finish = []
searchTerm: string;
alterViewNotNetWork = false

task_seccion = "task_new"

items = [
  'Amsterdam',
  'Bogota',
];

request = {
  word: ''
}


imoveis = [];
imoveis_filter = [];
imoveis_filter_next_page: string;
filter: boolean = false;
load: Loading;
nextpage: any;
lastpage: any;

url: string = 'http://34.68.95.171/mapeamento_api/public/api/';
url2: string = 'http://localhost:8000/api/';

//fim -variaveis globais

constructor(
  public navCtrl: NavController,
  public navParams: NavParams,
  public loadingCtrl: LoadingController,
  public modalCtrl: ModalController,
  public alertCtrl: AlertController,
  public http: HttpClient,
) {

  this.loadingPresent()

  http.get(this.url + "imoveis").subscribe((res) => {
    this.load.dismiss()
    this.imoveis = res["data"]
    this.nextpage = res["next_page_url"]
    console.log('next page', this.nextpage)
    console.log(res)
  }, error => {
    this.load.dismiss() 
    console.log(error)
   }
  )

}

//métodos aqui
filterItems(){
  this.loadingPresent()
  console.log(this.searchTerm)

  if(this.searchTerm != ''){
    this.filter = true
  } else {
    this.filter = false
  }


  this.request.word = this.searchTerm.toUpperCase()
  this.http.post(this.url + "imoveisbyname",this.request).subscribe((data) => {
    console.log(data)
    this.imoveis_filter = data[0]["data"];
    this.imoveis_filter_next_page = data[0]["next_page_url"];


    //arrayBairros.forEach(element => {
    //  this.imoveis.push(element)
    //});
    this.load.dismiss()
  }, error => {
    this.load.dismiss()
    console.log(error)
   })
}

cancelSearch() {
  console.log('cancel search')
}




doInfinite(infinity) {
  if(this.nextpage != null){
    this.http.get(this.nextpage).subscribe((res) => {
      console.log('data',res)
      this.nextpage = res['next_page_url'];
      infinity.complete();

      let new_array =  res["data"];

      new_array.forEach(element => {
        this.imoveis.push(element)
      });
      
      // let arrayBairros = data[0]['data'];


      // arrayBairros.forEach(element => {
      //   this.imoveis.push(element)
      // });

    }, error => {
      infinity.complete()
      console.log(error)
     })
  }else {infinity.complete()}

}

doInfiniteFilter(infinity) {
  if(this.nextpage != null){
    this.http.post(this.imoveis_filter_next_page + '&word=' + this.request.word, this.request.word).subscribe((data) => {
      this.imoveis_filter_next_page = data[0]['next_page_url'];
      console.log(data)
      infinity.complete()
      let arrayBairros = data[0]['data'];


      arrayBairros.forEach(element => {
        this.imoveis_filter.push(element)
      });

    }, error => {
      infinity.complete()
      console.log(error)
     })
  }else {infinity.complete()}

}

loadingPresent(){
  this.load = this.loadingCtrl.create({
    content: "carregando dados.."
  })

  this.load.present()
}

openMap(item){
  //console.log(item)
  this.navCtrl.push('MapsPage', {lat:item["lat"], long: item["long"]})
}


searchByCity() {
  console.log('search by city')
  console.log(this.searchTerm)

  this.items;

  this.items.forEach(element => {
    if (element.toLowerCase() == this.searchTerm.toLowerCase()) {
      console.log("item", element)
    } else {
      console.log("nenhum elemento encontrado com esse nome")
    }
  })
}


//ate aqui - metodos
}