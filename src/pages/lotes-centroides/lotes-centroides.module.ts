import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LotesCentroidesPage } from './lotes-centroides';

@NgModule({
  declarations: [
    LotesCentroidesPage,
  ],
  imports: [
    IonicPageModule.forChild(LotesCentroidesPage),
  ],
})
export class LotesCentroidesPageModule {}
