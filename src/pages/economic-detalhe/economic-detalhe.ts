import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController, Loading, LoadingController } from 'ionic-angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions,
  ILatLng,
  Circle,
} from '@ionic-native/google-maps';
import { HttpClient } from '@angular/common/http';
/**
 * Generated class for the EconomicDetalhePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-economic-detalhe',
  templateUrl: 'economic-detalhe.html',
})
export class EconomicDetalhePage {

  item: any
  map: GoogleMap;

  position = {
    lat: 0,
    lng: 0
  }
  url: string = 'http://34.68.95.171/mapeamento_api/public/api/';

  economico: any

  constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public alertCtrl: AlertController,
     public modalCtrl: ModalController,
     public http: HttpClient,
     public loadingCtrl: LoadingController
     ) {
    console.log('EconomicDetalhePage')
    this.item = this.navParams.get('item')
    console.log(this.item)

    //this.loadGoogleMap()
    //this.loadMap()
  }

  ionViewDidLoad(){
    let Loading = this.loadingCtrl.create({content:'Carregando'})
    Loading.present();
    this.http.post(this.url+'economicobybql', {bql: this.item.bql})
      .subscribe(res => {
        this.economico = res[0]
        console.log(res)
        Loading.dismiss();
      })
  }

 /* loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.item.lat,
          lng: this.item.long
        },
        zoom: 18,
        tilt: 30
      }
    };

    let GOOGLE: ILatLng = {"lat" :this.item.lat, "lng" : this.item.long};
    this.map = GoogleMaps.create('map_canvas', mapOptions);

    this.map.one(GoogleMapsEvent.MAP_READY)
    .then(() => {
        // Add circle
        this.map.addCircle({
          'center': GOOGLE,
          'radius': 250,
          'strokeColor' : 'rgba(0, 0, 255, 0.75)',
          'strokeWidth': 1,
          'fillColor' : 'rgba(0, 0, 255, 0.1)',
          'visible': true
        }).then((circle: Circle) => {
          this.map.addMarker({
            title: 'Localização atual',
            icon: 'red',
            animation: 'DROP',
            position: {
              lat: this.item.lat,
            lng: this.item.long
            }
          })
            .then(() => {
       
            }).catch((err) => {
              
              console.log(err)
            })
        });


    }).catch(err => {console.log('err 1', err)})
  

  }
  
  loadGoogleMap() {
    let load = this.loadingCtrl.create({
      content: 'Aguarde...'
    })

    load.present()
    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: this.item.lat,
          lng: this.item.long
        },
        zoom: 18,
        tilt: 30
      }
    };

    console.log('tudo ok 1')
    this.map = GoogleMaps.create('map_canvas', mapOptions);
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('tudo ok 2')

        this.map.addMarker({
          title: 'Localização atual',
          icon: 'red',
          animation: 'DROP',
          position: {
            lat: this.item.lat,
          lng: this.item.long
          }
        })
          .then(() => {
            load.dismiss()
          }).catch((err) => {
            load.dismiss()
            console.log(err)
          })

      }).catch(err => {console.log('err 1', err), load.dismiss()})
  }
*/
  
  /*showAlert() {
    const alert = this.alertCtrl.create({
      
      title: 'Lat,Lon',
      message: this.item['lat'] + `<br>` + this.item['long'],
      buttons: [{text: 'OK', role: 'cancel'}]
    });
    alert.present();
  }*/

 /*openMap(item){
   console.log('Enviando dados para  MapsPage', item["lat"], item["long"])
   this.navCtrl.push('MapsPage', {lat:item["lat"], long: item["long"]})
 }*/

  sugestao(){
    console.log('função de sugestao')
    alert('teste')
    //this.navCtrl.push('SugestoesPage',{item:this.item})
  }
  

}