import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EconomicDetalhePage } from './economic-detalhe';
import { BrMaskerModule } from 'brmasker-ionic-3';

@NgModule({
  declarations: [
    EconomicDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(EconomicDetalhePage),
    BrMaskerModule
  ],
})
export class EconomicDetalhePageModule {}
