import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ComercioDetalhePage } from './comercio-detalhe';

@NgModule({
  declarations: [
    ComercioDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(ComercioDetalhePage),
  ],
})
export class ComercioDetalhePageModule {}
