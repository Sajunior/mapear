import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ModalController } from 'ionic-angular';

/**
 * Generated class for the ComercioDetalhePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-comercio-detalhe',
  templateUrl: 'comercio-detalhe.html',
})
export class ComercioDetalhePage {

  item: any

  constructor(
     public navCtrl: NavController,
     public navParams: NavParams,
     public alertCtrl: AlertController,
     public modalCtrl: ModalController
     ) {
    this.item = this.navParams.get('item')
    console.log(this.item)
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad ComercioDetalhePage');
  }
  
  showAlert() {
    const alert = this.alertCtrl.create({
      
      title: 'Lat,Lon',
      message: this.item['lat'] + `<br>` + this.item['long'],
      buttons: [{text: 'OK', role: 'cancel'}]
    });
    alert.present();
  }

  openMap(){
    this.navCtrl.push('MapsPage', {lat:this.item['lat'], long:this.item['long']});


  }

}
