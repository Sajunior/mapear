import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PropertiesDetalhePage } from './properties-detalhe';

@NgModule({
  declarations: [
    PropertiesDetalhePage,
  ],
  imports: [
    IonicPageModule.forChild(PropertiesDetalhePage),
  ],
})
export class PropertiesDetalhePageModule {}
